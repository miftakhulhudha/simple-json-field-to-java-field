import java.io.File;
import java.util.Scanner;

/**
 * @author miftakhul hudha
 * @version 0.1
 * 
 * parsing json field to java field
 */
public class JsonUtil {

    public static int SNAKE_CASE = 1;
    public static int SCREAMING_SNAKE_CASE = 2;

    public static void main(String[] args) throws Exception{
        System.out.println("Starting");

        String filePath = args[0];
        File textFile = new File(filePath);

        if (!textFile.exists()) {
            System.out.println("File doesnt exist");
            return;
        }


        System.out.println("Format yg didukung");
        System.out.println("1. snake_case");
        System.out.println("2. SCREAMING_SNAKE_CASE");
        System.out.println("Tentukan format : (input nomor format)");

        try {
            Scanner command = new Scanner(System.in);

            int mode = command.nextInt();

            while(mode > 2)
            {
                System.out.println("Tentukan format yg didukung :");            
                mode = command.nextInt();
            }
                

            System.out.println("==============================");
            System.out.println("Sedang memproses");
            System.out.println("==============================");
            execute(textFile, mode);
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan");
        }

        

        
    }

    static void execute(File textFile, int mode) throws Exception{
        System.out.println("Scanning file");
        System.out.println("==============================");
        Scanner textScanner = new Scanner(textFile);
        while (textScanner.hasNextLine()) {
            String row = textScanner.nextLine();
            row = row.replaceAll("\\s+", "");

            String[] datas = row.split("\":");

            String rawSplittedString = "private String "+datas[0] + ";"; 

            String removedPetik = rawSplittedString.replace("\"", "");


            // format to camel
            String[] findUnderscore = removedPetik.split("_");
            
            String result = "";
            switch (mode) {
                case 1: {
                    result = snakeCase(findUnderscore);
                    break;
                }
                case 2:
                    result = screameSnakeCase(findUnderscore);
                    break;
            }

            System.out.println(result);
        }
    }

    static String snakeCase(String[] splitedUnderScore) {
        StringBuilder compltedBuilder = new StringBuilder();
        for (int i = 0; i<splitedUnderScore.length; i++) {
            if (i == 0) {
                compltedBuilder.append(splitedUnderScore[i]);
                continue;
            }

            String current = splitedUnderScore[i];
            StringBuffer buffer = new StringBuffer(current);
            
            StringBuilder sBuilder = new StringBuilder();
            for (int b=0; b<buffer.length(); b++) {
                if (b == 0) {
                    String car = String.valueOf(Character.toUpperCase(buffer.charAt(b)));
                    sBuilder.append(car);
                    continue;
                }
                sBuilder.append(buffer.charAt(b));
            }
            String sfinal = sBuilder.toString();
            compltedBuilder.append(sfinal);
        }
        return compltedBuilder.toString();
    }

    static String screameSnakeCase(String[] splitedUnderScore) {

        StringBuilder compltedBuilder = new StringBuilder();
        for (int i = 0; i<splitedUnderScore.length; i++) {
            if (i == 0) {
                compltedBuilder.append(splitedUnderScore[i].toLowerCase());
                continue;
            }

            String current = splitedUnderScore[i];
            StringBuffer buffer = new StringBuffer(current);
            
            StringBuilder sBuilder = new StringBuilder();
            for (int b=0; b<buffer.length(); b++) {
                if (b == 0) {
                    String car = String.valueOf(Character.toUpperCase(buffer.charAt(b)));
                    sBuilder.append(car);
                    continue;
                } else {
                    String car = String.valueOf(Character.toLowerCase(buffer.charAt(b)));
                    sBuilder.append(car);
                }                
            }
            String sfinal = sBuilder.toString();
            compltedBuilder.append(sfinal);
        }
        return compltedBuilder.toString();
    }

}